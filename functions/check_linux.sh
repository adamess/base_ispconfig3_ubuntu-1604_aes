#---------------------------------------------------------------------
# Function: CheckLinux
#    Check Installed Linux Version
#---------------------------------------------------------------------

CheckLinux() {

  #Extract information on system
  . /etc/os-release

  # Set DISTRO variable to null
  DISTRO=''
  
  #---------------------------------------------------------------------
  #	Ubuntu 16.04
  #---------------------------------------------------------------------

  if echo $ID-$VERSION_ID | grep -iq "ubuntu-16.04"; then
		DISTRO=ubuntu-16.04
  fi

}

